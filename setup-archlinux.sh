#! /usr/bin/env sh

# Fenix ISO Builder
# Please refer to the file `LICENSE` in the main directory for license information.
# For a high level documentation, please visit https://gitlab.com/rebornos-team/fenix/fenix-iso-builder

# AUTHORS
# 1. shivanandvp@rebornos.org
# 2.

# Install Python dev tools
echo [ Fenix ISO Builder Initial Setup for Arch Linux ]
echo "Installing \`python\` and \`pip\`..."
sudo pacman -S --needed python python-pip
echo "Installing \`pipenv\` through \`pip\`"...
sudo pip install pipenv

# Install other packages
echo "Installing \`git\`, \`linux-headers\`, and \`archiso\`..."
sudo pacman -S --needed git linux-headers archiso

# Install runtime dependencies
echo "Installing runtime dependencies..."
sudo pipenv install

# Adding permissions
echo "Giving executable permissions to \`fenix-iso-builder.sh\`..."
sudo chmod +x fenix-iso-builder.sh

echo ""
echo "\nSetup completed. Please refer to the above messages for any errors..."
