.. _contributing:

How to Contribute to Fenix ISO Builder
**************************************

Testing
=======

To test, all you need to do is follow the instructions in :ref:`prerequisites` and :ref:`usage` to run **Fenix ISO Builder**, and boot the generated *ISO* on either a virtual machine or on a real computer. 

Development
===========

1. **Runtime Prerequisites**: Follow the instructions in :ref:`prerequisites`.
   
2. **Packages**: Install the Arch Linux packages :code:`base-devel`, :code:`graphviz`, and :code:`fontconfig`.

    .. code-block:: bash

        sudo pacman -S --needed base-devel graphviz fontconfig

3. **Dev Dependencies**: Run the following commands on a terminal after changing to the project directory: 

    .. code-block:: bash

        cd fenix-iso-builder
        sudo pipenv install --system --dev

4. **API Documentation**: Consult :ref:`api_documentation` for an overview of the code.
   
5. **Git Repository**: *Fork* or *clone* the project from its **Gitlab** page and edit the source code of the project. For the *Git* URL, please refer to :ref:`prerequisites`. 
   
6. **Code Editor**: Make sure that you have a code editor installed (like :code:`vscode`, :code:`atom`, :code:`gedit`, or an IDE like :code:`pycharm`). We recommend *VSCode* since if you open the file :code:`iso_builder.code-workspace` (found in the directory :code:`fenix-iso-builder/`) in *VSCode*, the :underlined:`recommended` *extensions* and *settings* are preconfigured to make your job easy and less error prone.

    .. code-block:: bash

        sudo pacman -S --needed code
   
7. **Terminal**: Of course, you also need a terminal to run commands and to debug. 