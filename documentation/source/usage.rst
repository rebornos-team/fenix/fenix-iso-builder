.. _usage:

Usage
*****

**Warning**: Make sure that you have read and followed the steps in :ref:`prerequisites` before proceeding.

Cleanup of old files
====================

If this is :underlined:`not` the first time running **Fenix ISO Builder** on your system, please clear out your *build*, *work*, and *out* directories using one of the two methods below. Otherwise, proceed to the next section :ref:`running_fenix_iso_builder`.

Automatic Cleanup
^^^^^^^^^^^^^^^^^

Edit the file :code:`fenix-iso-builder/configuration/live_iso.json` to change

.. code-block:: JSON

    "clear_directories": false,

to 

.. code-block:: JSON

    "clear_directories": true,

**Warning** Automatic cleanup will automatically delete everything in the *build*, *work*, and *out* directories. The actual path of those directories is listed in :code:`fenix-iso-builder/configuration/live_iso.json`.

Manual Cleanup
^^^^^^^^^^^^^^^^^

**Warning** Please be very careful in using `sudo rm -rf <directory>` as it can damage your file system if not typed correctly.

Manually clear directories listed as :code:`"build_directory"`, :code:`"work_directory"`, and :code:`"output_directory"` whose paths are listed in :code:`fenix-iso-builder/configuration/live_iso.json`. An example of how to remove the default build directory is 

.. code-block:: bash

    sudo rm -rf /tmp/rebornos-live/

.. _running_fenix_iso_builder:

Running - Launching Fenix ISO Builder
=====================================

On a terminal, ensure that you are in the project base directory (:code:`fenix-iso-builder/`) and run: 

.. code-block:: bash

    ./fenix-iso-builder.sh

**Note**: If the ISO builder fails due to insufficient space, please change the :code:`"work_directory"` and :code:`"output_directory"` in :code:`fenix-iso-builder/configuration/live_iso.json` to another path, like :code:`"/home/<your_username>/ISO/"`. The :code:`/tmp/` directory is usually limited in space and depending on individual setups, it may fill up when **Fenix ISO Builder** is working.

Logging - Checking for progress and issues
==========================================

After you launch **Fenix ISO Builder**, you can keep track of the progress and check for any errors by monitoring the log. You could do it in the following ways:

    * Open in an editor: Find the latest log in :code:`fenix-iso-builder/log/` and open it in an editor. 

    OR

    * Print the latest messages: Run :code:`tail -f $( ls -t1 |  head -n 1 )` on a terminal after changing to the :code:`fenix-iso-builder/log/` directory.

The Output - Where to find the generated ISO
============================================

If **Fenix ISO Builder** completes running and successfully generates an *ISO* (verfied by checking the logs as explained above), then the path where the final ISO will be found, is specified as the *value* for the *key* :code:`"output_directory"` in the file :code:`fenix-iso-builder/configuration/live_iso.json`. A sample entry in the file would look like: :code:`"output_directory": "/tmp/rebornos-live/out/",`.