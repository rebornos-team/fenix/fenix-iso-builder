.. _configuration:

Configuration - Customizing the ISO builder
*******************************************
Configuration of the **Fenix ISO builder** is done through a :code:`JSON` file. It is similar to a Python :code:`dict`, except that **JSON** uses boolean values with the first letter not capitalized (for example, :code:`true` in **JSON** as opposed to :code:`True` in **Python**).
The configuration file is found at :code:`fenix-iso-builder/configuration/live_iso.json`

Below is a brief description of currently offered configuration variables:

:"build_directory":
    The absolute path to the directory in which you would like :code:`archiso` files to be copied before modifying them.

:"work_directory":
    The absolute path to the directory in which you would like :code:`mkarchiso` to install all the ISO packages and copies all the package caches before writing them all to an ISO.

:"output_directory":
    The absolute path to the directory in which would like :code:`mkarchiso` to store the final generated ISO.

:"clear_directories":
    * If "true", deletes the actual build_directory, work_directory, and output_directory, whose paths are specified in the file.
    * If "false", the build_directory, work_directory, and output_directory are :underlined:`not` deleted.

:"archiso_profile": One of "releng" or "baseline". 
    From the `Archiso Wiki <https://wiki.archlinux.org/index.php/Archiso#Prepare_a_custom_profile>`_ : 
    * :code:`releng` is used to create the official monthly installation ISO. It can be used as a starting point for creating a customized ISO image.
    * :code:`baseline` is a minimalistic configuration, that includes only the bare minimum packages required to boot the live environment from the medium.

:"architecture":
    The system architecture. For most modern computers, it should be "x86_64"

:"mirror_url":
    The Arch Linux mirror URL, usually at "https://mirrors.kernel.org/archlinux"

:"archiso_base":
    The base directory of :code:`archiso`, which is usually at "/usr/share/archiso/configs/"

:"use_startup_script":
    * If "true", the script specified in **"startup_script_path"** will be run at boot time of the ISO before login and before a user-interace is loaded.
    * If "false", no custom script is run at boot

:"startup_script_path":
    The absolute path to a startup script (a shell script) that will be run at boot time of the ISO before login and before a user-interace is loaded.

:"additional_iso_packages":
    A list of packages that the user would like to install on the live ISO in addition to what `archiso` installs by default according to the "archiso_profile" chosen above. 

:"use_cache_packages":
    * If "true", packages are cached on the ISO for pacman to pull during a potential installation on the target machine.
    * If "false", no packages are cached on the ISO for pacman to access

:"packages_to_be_cached":
    A list of packages to be cached on the ISO for pacman to pull during a potential installation on the target machine.

:"relative_database_cache_path":
    The relative path to which the pacman *database cache* is copied for offline installation of packages. It is usually "var/lib/pacman/sync/"

:"relative_package_cache_path":
    The relative path to which the pacman *package cache* is copied for offline installation of packages. It is usually "var/cache/pacman/pkg/"