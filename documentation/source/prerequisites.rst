.. _prerequisites:

Prerequisites - What you need to run Fenix ISO Builder
******************************************************

**Note**: Currently, **Fenix ISO Builder** only supports **Arch Linux** and its derivatives (like *RebornOS*, *EndeavourOS*, *Manjaro*, etc.). Your build system (where you run **Fenix ISO Builder**) should be booted into  **Arch Linux** or one of its derivates before proceeding.  

Follow :underlined:`one` of the below sections to set up **Fenix ISO Builder** before running. The below commands must be run on a **terminal**.

Using the Setup Script
======================

1. **Download the project**:
    
    .. code-block:: bash

        git clone https://gitlab.com/rebornos-team/fenix/fenix-iso-builder.git

2. **Run the setup script**:  

    .. code-block:: bash

        cd fenix-iso-builder
        sh setup-archlinux.sh
        
If the script ran successfully, congratulations! You are ready to run **Fenix ISO Builder**. Proceed to :ref:`usage` for further steps.

Manual Setup
============

**Warning**: Please skip this section if you have already run the setup script successfully.

1. **Install python dev tools**: 

    * Install the packages :code:`python` and :code:`pip`

    .. code-block:: bash
        
        sudo pacman -S --needed python python-pip

    * Install :code:`pipenv` through :code:`pip`

    .. code-block:: bash
        
        sudo pip install pipenv

3. **Install other packages**: Install :code:`git`, :code:`linux-headers`, and :code:`archiso`

    .. code-block:: bash
            
        sudo pacman -S --needed git linux-headers archiso

4. **The program itself**: Download **Fenix ISO Builder** into a directory of your choice

    .. code-block:: bash

        git clone https://gitlab.com/rebornos-team/fenix/fenix-iso-builder.git

5. **Runtime dependencies**: Install the *python modules* required by **Fenix ISO Builder**

    .. code-block:: bash

        cd fenix-iso-builder
        sudo pipenv install

6. **Executable permissions**: Provide executable permissions to the ISO builder script

    .. code-block:: bash

        sudo chmod +x fenix-iso-builder.sh

If you performed all the above steps successfully, congratulations! You are ready to run **Fenix ISO Builder**. Proceed to :ref:`usage` for further steps.

