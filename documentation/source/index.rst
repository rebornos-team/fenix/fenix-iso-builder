.. _index:

.. Fenix ISO Builder documentation master file, created by
   sphinx-quickstart on Tue Feb 16 09:01:25 2021.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to Fenix ISO Builder's documentation!
*********************************************

*Documentation by:*
   * *shivanandvp* *(shivanandvp@rebornos.org)*
   * *elkrien*

.. toctree::
   :maxdepth: 7
   :caption: Help:
   
   Welcome <self>
   Prerequisites <prerequisites>
   Usage <usage>
   Customization <configuration>
   Contributing <contributing>

.. toctree::
   :maxdepth: 2
   :caption: API Documentation:

   Fenix ISO Builder <api_documentation>

Fenix ISO Builder: the What, Why, and How
=========================================

**Fenix ISO builder** is a tool that generates a live installation medium for Arch Linux and its derivatives. 
It fetches a fresh copy of the :code:`archiso` files present in your system, and modifies it based on user-defined settings, before using :code:`mkarchiso` to build a live medium.

The user-defined settings can be conveniently edited via a :code:`JSON` file. The progress as well as the output of the tool can be monitored by checking the logs.

.. Indices and tables
.. ==================

.. * :ref:`genindex`
.. * :ref:`modindex`
.. * :ref:`search`
