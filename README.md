# Fenix ISO Builder - RebornOS

*README by @shivanandvp (shivanandvp@rebornos.org)*

*Please refer to [`LICENSE`](./LICENSE) for license information.*

## Overview

Fenix ISO builder is a tool that generates a live installation medium for Arch Linux and its derivatives. 
It fetches a fresh copy of the `archiso` files present in your system and modifies it according to custom settings, before using `mkarchiso` to build the live medium.
Settings are conveniently edited via a `JSON` file. The progress as well as the output of the tool can be monitored by checking the logs.

## [PLEASE CLICK HERE](https://rebornos-team.gitlab.io/fenix/fenix-iso-builder/index.html) for the full documentation

## Fenix ISO Builder Logs in Code - OSS

![](media/screenshots/1.png)
![](media/screenshots/2.png)
![](media/screenshots/3.png)
![](media/screenshots/4.png)
![](media/screenshots/5.png)
![](media/screenshots/6.png)

## The generated Live ISO booted in VirtualBox

![](media/screenshots/7.png)