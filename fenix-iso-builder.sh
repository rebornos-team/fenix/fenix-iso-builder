#! /usr/bin/env sh

# Fenix ISO Builder
# Please refer to the file `LICENSE` in the main directory for license information.
# For a high level documentation, please visit https://gitlab.com/rebornos-team/fenix/fenix-iso-builder

# AUTHORS
# 1. shivanandvp@rebornos.org
# 2.

# Run the python file in a virtual environment
sudo pipenv run python3 main.py