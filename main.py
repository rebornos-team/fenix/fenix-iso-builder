#! /usr/bin/python

# Fenix ISO Builder
# Please refer to the file `LICENSE` in the main directory for license information.
# For a high level documentation, please visit https://gitlab.com/rebornos-team/fenix/fenix-iso-builder

# AUTHORS
# 1. shivanandvp@rebornos.org
# 2.

# This is the Python entry point of the iso-builder
# Please edit configuration/live_iso.json to customize the ISO generation

# IMPORTS
from argparse import Namespace
import os
import logging
import datetime
import subprocess
import pathlib
from logging import Logger
from abc import ABC  # For declaring abstract classes
from typing import List, Tuple
import logging
import copy
from elevate import elevate
from concurrent.futures import ThreadPoolExecutor
import re
import textwrap
from typing import Optional

# FENIX IMPORTS
from fenix_library.configuration import JSONConfiguration # For reading and writing settings files
from fenix_library.running import LogMessage, Command, LoggingHandler # For running OS commands

class FenixISOBuilder(ABC):
    """
    An internal-use class to encapsulate the tasks associated with building an ISO for Fenix Installer

    """

    def __init__(self) -> None:
        """
        The main function.

        The following tasks are accomplished:
        - Configure logging
        - Set the current working directory,

        Parameters
        ----------
        None

        Returns
        -------
        Nothing
        """

        self.logfile_path: pathlib.Path
        (logger, logfile_path) = self.setup_logger()
        self.logging_handler: LoggingHandler = LoggingHandler([logger.log])
        LogMessage.Info("Logging output in " + str(logfile_path) + "...").write(self.logging_handler)
        # get ready to access the ISO settings from the configuraton file
        iso_settings = JSONConfiguration("configuration/live_iso.json")
        # set the base directory of the installer as the current working directory
        self.set_current_working_directory()

        elevate(graphical=False)  # Acquire superuser privileges

        # check for potentially unsafe paths
        self.validate_paths(iso_settings)
        # cleanup directories
        if iso_settings["clear_directories"]:
            self.cleanup_directories(iso_settings)
        # copy the archiso files to the build directory specified in the configuration file (configuration/live_iso.json)
        self.copy_archiso_to_build_directory(iso_settings)
        # add packages specified by the user (in the configuration file) to the list of packages to be installed on the live ISO
        self.append_to_iso_package_list(iso_settings)
        if iso_settings["use_cache_packages"]:
            # download the packages required for caching and move them to the ISO build directory
            self.download_and_move_packages_for_caching(iso_settings)
        if iso_settings["use_startup_script"]:
            # set the startup script (if it exists) to run while booting into the ISO
            self.setup_startup_script(iso_settings)
        self.build_iso(iso_settings)  # build the ISO using mkarchiso

    def validate_paths(self, iso_settings: JSONConfiguration) -> None:
        """
        Check if the build, work, or output paths specified in `live_iso.json` seem unsafe and exit if they can be unsafe.

        Parameters
        ----------
        iso_settings: JSONConfiguration
            The user-specified settings for the live ISO

        Returns
        -------
        Nothing
        """          

        errors_encountered: bool = False
        
        for path_selection in [
            "build_directory",
            "work_directory",
            "output_directory"
        ]: 
            if not iso_settings[path_selection].endswith("/"): # Append "/" to the directory path if it does not end with that character
                iso_settings[path_selection] = iso_settings[path_selection] + "/"

            path_string = iso_settings[path_selection]
            path_object = pathlib.Path(path_string)
            parent_directory_list = [
                parent_object.name
                for parent_object in path_object.parents
            ]
            number_of_parent_directories = len(parent_directory_list)
            if number_of_parent_directories < 3: # If there are less than three levels of parent directories
                if(
                    number_of_parent_directories >= 2
                    and parent_directory_list[-2] != "tmp"
                ) or number_of_parent_directories < 2:
                    LogMessage.Error(
                        "The path " + path_string + " seems unsafe. Please check `configuration/live_iso.json` and change this path before trying again. Hint: Try creating a subdirectory and use that instead."
                    ).write(self.logging_handler)
                    errors_encountered = True
        
        if errors_encountered:
            exit(2) # Exit the application with an error code, if at least one the paths seemed unsafe


    def cleanup_directories(self, iso_settings: JSONConfiguration) -> None:
        """
        Clear out the build, work, and output directories.

        Parameters
        ----------
        iso_settings: JSONConfiguration
            The user-specified settings for the live ISO

        Returns
        -------
        Nothing
        """

        LogMessage.Info("[ CLEANUP ]").write(self.logging_handler)
                
        LogMessage.Info(
            "Clearing the build directory at "
            + iso_settings["build_directory"]
            + "..."
        ).write(self.logging_handler)
        Command(
            [
                "rm", "-rf", iso_settings["build_directory"]
            ]
        ).run_log_and_wait(self.logging_handler)

        LogMessage.Info(
            "Clearing the work directory at "
            + iso_settings["work_directory"]
            + "..."
        ).write(self.logging_handler)
        Command(
            [
                "rm", "-rf", iso_settings["work_directory"]
            ]
        ).run_log_and_wait(self.logging_handler)

        LogMessage.Info(
            "Clearing the output directory at "
            + iso_settings["output_directory"]
            + "..."
        ).write(self.logging_handler)
        Command(
            [
                "rm", "-rf", iso_settings["output_directory"]
            ]
        ).run_log_and_wait(self.logging_handler)
        

    def copy_archiso_to_build_directory(self, iso_settings: JSONConfiguration) -> None:
        """
        Copy the archiso files to the build directory specified in the configuration file (configuration/live_iso.json)

        Parameters
        ----------
        iso_settings: JSONConfiguration
            The user-specified settings for the live ISO

        Returns
        -------
        Nothing
        """

        LogMessage.Info("[ COPY ARCHISO FILES ]").write(self.logging_handler)
        archiso_path: str = iso_settings["archiso_base"] + \
            iso_settings["archiso_profile"].lower() + "/."
        LogMessage.Info("Archiso path: " +
                        archiso_path).write(self.logging_handler)
        LogMessage.Info("Build directory : " +
                        iso_settings["build_directory"]).write(self.logging_handler)
        LogMessage.Info("Creating the build directory if it does not exist...").write(
            self.logging_handler)
        os.makedirs(os.path.dirname(
            iso_settings["build_directory"]), exist_ok=True)
        LogMessage.Info("Initiating copy of archiso files...")
        Command(
            [
                "cp", "-r", archiso_path, iso_settings["build_directory"]
            ]
        ).run_log_and_wait(self.logging_handler)

    def append_to_iso_package_list(self, iso_settings: JSONConfiguration) -> None:
        """
        Add packages specified by the user (in the configuration file configuration/live_iso.json) to the list of packages to be installed on the live ISO

        Parameters
        ----------
        iso_settings: JSONConfiguration
            The user-specified settings for the live ISO

        Returns
        -------
        Nothing
        """

        LogMessage.Info("[ UPDATE ISO PACKAGE LIST ]").write(
            self.logging_handler)

        archiso_path: str = iso_settings["archiso_base"] + \
            iso_settings["archiso_profile"].lower() + "/"
        iso_package_list_file_path: str = archiso_path + \
            "packages" + "." + iso_settings["architecture"]
        LogMessage.Info("ISO Package List File Path : " +
                        iso_package_list_file_path).write(self.logging_handler)
        LogMessage.Info("Additional packages : " +
                        str(iso_settings["additional_iso_packages"])).write(self.logging_handler)

        LogMessage.Info("Reading existing packages from the package list file...").write(
            self.logging_handler)
        packages = []
        with open(iso_package_list_file_path, 'r') as iso_package_list_file:
            package_str = iso_package_list_file.read()
            packages = package_str.split("\n")

        LogMessage.Info("Merging ISO package lists and eliminating duplicates...").write(
            self.logging_handler)
        packages.extend(iso_settings["additional_iso_packages"])
        package_dict = dict.fromkeys(packages)
        try:
            [package_dict.pop(key) for key in iso_settings["remove_iso_packages"]]
        except Exception:
            pass
        packages = list(package_dict)
        LogMessage.Info("Package list after removing duplicates and marked packages: " + str(packages)).write(self.logging_handler)

        LogMessage.Info("Updating the package list file...").write(
            self.logging_handler)
        with open(iso_package_list_file_path, 'w') as iso_package_list_file:
            iso_package_list_file.write("\n".join(packages))

    def gather_dependencies(self, packages_to_be_cached) -> List[str]:
        detailed_packages = copy.deepcopy(packages_to_be_cached)
        for package in packages_to_be_cached:
            LogMessage.Debug("Gathering dependencies for " +
                             package + "...").write(self.logging_handler)
            string_dependencies = Command(
                [
                    "pactree", "--ascii", "--linear", "--sync", "--unique", package
                ]
            ).run_and_wait()
            detailed_packages.extend(
                string_dependencies.strip().split("\n")
            )
        detailed_packages = list(dict.fromkeys(
            detailed_packages))  # Eliminate duplicates
        return detailed_packages

    def download_and_move_packages_for_caching(self, iso_settings: JSONConfiguration) -> None:
        """
        Download the packages required for caching and move them to the ISO build directory

        Parameters
        ----------
        iso_settings: JSONConfiguration
            The user-specified settings for the live ISO

        Returns
        -------
        Nothing
        """

        if not iso_settings["use_cache_packages"]:
            return

        LogMessage.Info("[ CACHE PACKAGES ]").write(self.logging_handler)
        LogMessage.Info("Updating database list...").write(
            self.logging_handler)
        Command(
            [
                "sudo", "pacman", "-Sy"
            ]
        ).run_log_and_wait(self.logging_handler)
        database_cache_path = iso_settings["build_directory"] + \
            "airootfs" + "/" + iso_settings["relative_database_cache_path"]
        LogMessage.Info("Database Cache Path: " +
                        database_cache_path).write(self.logging_handler)
        LogMessage.Info("Creating a directory at the database cache directory path if it does not exist...").write(
            self.logging_handler)
        os.makedirs(os.path.dirname(database_cache_path), exist_ok=True)
        LogMessage.Info("Downloading database files... [community.db]").write(
            self.logging_handler)
        url = iso_settings["mirror_url"] + "/community/os/" + \
            iso_settings["architecture"] + "/community.db"
        Command(
            [
                "wget", "-nv", "-P", database_cache_path, url
            ]
        ).run_log_and_wait(self.logging_handler)
        LogMessage.Info("Downloading database files... [core.db]").write(
            self.logging_handler)
        url = iso_settings["mirror_url"] + "/core/os/" + \
            iso_settings["architecture"] + "/core.db"
        Command(
            [
                "wget", "-nv", "-P", database_cache_path, url
            ]
        ).run_log_and_wait(self.logging_handler)
        LogMessage.Info("Downloading database files... [extra.db]").write(
            self.logging_handler)
        url = iso_settings["mirror_url"] + "/extra/os/" + \
            iso_settings["architecture"] + "/extra.db"
        Command(
            [
                "wget", "-nv", "-P", database_cache_path, url
            ]
        ).run_log_and_wait(self.logging_handler)
        LogMessage.Info("Downloading database files... [multilib.db]").write(
            self.logging_handler)
        url = iso_settings["mirror_url"] + "/multilib/os/" + \
            iso_settings["architecture"] + "/multilib.db"
        Command(
            [
                "wget", "-nv", "-P", database_cache_path, url
            ]
        ).run_log_and_wait(self.logging_handler)

        package_cache_path = iso_settings["build_directory"] + \
            "airootfs" + "/" + iso_settings["relative_package_cache_path"]
        LogMessage.Info("Package Cache Path: " +
                        package_cache_path).write(self.logging_handler)
        LogMessage.Info("Creating a directory at the package cache directory path if it does not exist...").write(
            self.logging_handler)
        os.makedirs(os.path.dirname(package_cache_path), exist_ok=True)
        LogMessage.Info(
            "The packages to be cached are (excluding dependencies) [N = "
            + str(len(iso_settings["packages_to_be_cached"]))
            + "]: "
            + str(iso_settings["packages_to_be_cached"])
        ).write(self.logging_handler)
        detailed_packages: List[str] = self.gather_dependencies(
            iso_settings["packages_to_be_cached"])
        LogMessage.Info(
            "The packages to be cached are [N = "
            + str(len(detailed_packages))
            + "]: "
            + str(detailed_packages)
        ).write(self.logging_handler)

        urls = []
        for package in detailed_packages:
            url = Command(
                [
                    "pacman", "-Sp", package
                ]
            ).run_and_wait().strip()
            urls.append(url)
            LogMessage.Info("Fetched URL for " + package +
                            ": " + url).write(self.logging_handler)

        urls = list(dict.fromkeys(urls))  # Eliminate duplicates

        for url in urls:
            LogMessage.Info("Downloading " + url.rsplit('/', 1)
                            [-1] + "... ").write(self.logging_handler)
            Command(
                [
                    "curl", "-O", url
                ],
                working_directory=package_cache_path
            ).run_and_wait()

    def setup_startup_script(self, iso_settings: JSONConfiguration) -> None:
        """
        Set the startup script (if it exists) to run while booting into the ISO    

        Parameters
        ----------
        iso_settings: JSONConfiguration
            The user-specified settings for the live ISO

        Returns
        -------
        Nothing
        """

        if not iso_settings["use_startup_script"]:
            return

        iso_startup_script_path = iso_settings["build_directory"] + \
            "airootfs" + "/" + "root" + "/" + ".iso_startup_script.sh"
        profiledef_path = iso_settings["build_directory"] + "profiledef.sh"

        LogMessage.Info("Copying the startup script...").write(
            self.logging_handler)
        Command(
            [
                "cp", "-f", iso_settings["startup_script_path"], iso_startup_script_path
            ]
        ).run_log_and_wait(self.logging_handler)

        LogMessage.Info("Giving permissions to the startup script...").write(
            self.logging_handler)

        profiledef_text = ""
        with open(profiledef_path, "r") as profiledef_file:
            profiledef_text = profiledef_file.read()

        profiledef_text = re.sub(
            r"file_permissions\s*=\s*\(",
            "file_permissions=(\n  [\"/root/.iso_startup_script.sh\"]=\"0:0:755\"",
            profiledef_text
        )
        profiledef_text = re.sub(
            r"iso_name\s*=\s*\".*?\"",
            "iso_name=\"RebornOS\"",
            profiledef_text
        )
        profiledef_text = re.sub(
            r"iso_label\s*=\s*\".*?\"",
            "iso_label=\"REBORNOS_$(date +%Y%m)\"",
            profiledef_text
        )
        profiledef_text = re.sub(
            r"iso_publisher\s*=\s*\".*?\"",
            "iso_publisher=\"RebornOS <https://www.rebornos.org>\"",
            profiledef_text
        )
        profiledef_text = re.sub(
            r"iso_application\s*=\s*\".*?\"",
            "iso_application=\"RebornOS Live/Recovery Medium\"",
            profiledef_text
        )
        profiledef_text = re.sub(
            r"iso_version\s*=\s*\".*?\"",
            "iso_version=\"$(date +%Y.%m.%d)\"",
            profiledef_text
        )

        with open(profiledef_path, "w") as profiledef_file:
            profiledef_file.write(profiledef_text)

        iso_startup_service_text = \
            """
        [Unit]
        Description=ISO Startup Script

        [Service]
        Type=oneshot
        ExecStart=/root/.iso_startup_script.sh
        RemainAfterExit=true

        [Install]
        WantedBy=multi-user.target
        """
        iso_startup_service_text = textwrap.dedent(iso_startup_service_text)

        iso_startup_service_path = iso_settings["build_directory"] + \
            "airootfs/etc/systemd/system/" + "iso_startup.service"
        iso_startup_service_symlink_directory_path = iso_settings["build_directory"] + \
            "airootfs/etc/systemd/system/multi-user.target.wants/"
        LogMessage.Info("Creating the required directories for the startup systemd service...").write(
            self.logging_handler)
        os.makedirs(os.path.dirname(iso_startup_service_path), exist_ok=True)
        os.makedirs(iso_startup_service_symlink_directory_path, exist_ok=True)
        LogMessage.Info("Creating a systemd service for the ISO startup...").write(
            self.logging_handler)
        with open(iso_startup_service_path, "w") as iso_startup_service_file:
            iso_startup_service_file.write(iso_startup_service_text)
        LogMessage.Info("Creating a symbolic link to the systemd service...").write(
            self.logging_handler)
        Command(
            [
                "ln", "-s", iso_startup_service_path, iso_startup_service_symlink_directory_path
            ]
        ).run_log_and_wait(self.logging_handler)

    def build_iso(self, iso_settings: JSONConfiguration) -> None:
        """
        Build the ISO using mkarchiso

        Parameters
        ----------
        iso_settings: JSONConfiguration
            The user-specified settings for the live ISO

        Returns
        -------
        Nothing
        """

        LogMessage.Info("Building the ISO...").write(self.logging_handler)

        # Make a copy of mkarchiso and modify it to preserve airootfs
        mkarchiso_path = Command(["which", "mkarchiso"]).run_and_wait()
        mkarchiso_path = mkarchiso_path.strip().replace('\n', '').replace('\t', '')
        Command(
            [
                "cp",
                "-p",
                mkarchiso_path,
                iso_settings["build_directory"]
            ]
        ).run_log_and_wait(self.logging_handler)
        mkarchiso_path = iso_settings["build_directory"] + "mkarchiso"
        mkarchiso_text = ""
        with open(mkarchiso_path, "r") as mkarchiso_file:
            mkarchiso_text = mkarchiso_file.read()
        mkarchiso_text = re.sub(
            r"_run_once\s*_cleanup_airootfs",
            "# _run_once _cleanup_airootfs",
            mkarchiso_text
        )
        with open(mkarchiso_path, "w") as mkarchiso_file:
            mkarchiso_file.write(mkarchiso_text)

        os.makedirs(os.path.dirname(
            iso_settings["work_directory"]), exist_ok=True)
        os.makedirs(os.path.dirname(
            iso_settings["output_directory"]), exist_ok=True)
        Command(
            [
                iso_settings["build_directory"] + "mkarchiso",
                "-v",
                "-w", iso_settings["work_directory"],
                "-o", iso_settings["output_directory"],
                iso_settings["build_directory"]
            ]
        ).run_log_and_wait(self.logging_handler)

    @staticmethod
    def setup_logger() -> Tuple[logging.Logger, pathlib.Path]:
        """
        Configure the logger

        The following tasks are accomplished:
        - Create a named logger
        - Setup logging to be done onto a file and the console
        - Define the format of log entries
        - Delete old log files

        Parameters
        ----------
        None

        Returns
        -------
        logger: logging.Logger
            The logger which has been set up
        logfile_path: pathlib.Path
            The path to the log file
        """

        # create a new logger and name it
        logger = logging.getLogger('fenix_iso_builder')

        FenixISOBuilder.delete_old_log_files(
            no_of_files_to_keep=5)  # delete old log files

        # set it to log anything of debugging and higher alert levels
        logger.setLevel(logging.DEBUG)

        # Set up file-based logging
        log_file_path = pathlib.Path(
            "log/" + "iso-builder-" + FenixISOBuilder.get_time_stamp() + ".log")
        log_file_handler = logging.FileHandler(
            log_file_path)  # for logging onto files
        # log debug messages and higher
        log_file_handler.setLevel(logging.DEBUG)
        # log_file_formatter = logging.Formatter('[%(asctime)s, %(levelname)-8s, %(name)s] %(message)s', '%Y-%m-%d, %H:%M:%S %Z') # old format of each log file entry
        log_file_formatter = logging.Formatter(
            '[%(asctime)s, %(levelname)-8s] %(message)s', '%Y-%m-%d, %H:%M:%S %Z')  # format of each log file entry
        log_file_handler.setFormatter(log_file_formatter)
        logger.addHandler(log_file_handler)

        # Set up standard console logging
        log_error_handler = logging.StreamHandler()  # for logging onto the console
        # log info messages and higher alert levels
        log_error_handler.setLevel(logging.INFO)
        log_error_formatter = logging.Formatter(
            '[%(levelname)8s!] %(message)s')  # format of each console log entry
        log_error_handler.setFormatter(log_error_formatter)
        logger.addHandler(log_error_handler)

        return (logger, log_file_path)

    @staticmethod
    def delete_old_log_files(no_of_files_to_keep: int) -> None:
        """
        Delete old log files while keeping the newest ones whose count is specified by "no_of_files_to_keep"

        Parameters
        ----------
        no_of_files_to_keep: int
            The number of newest log files to keep

        Returns
        -------
        Nothing        
        """

        subprocess.Popen(
            "ls -tp iso-builder* | grep -v '/$' | tail -n +"
            + str(no_of_files_to_keep + 1)
            + " | xargs -I {} rm -- {}",
            shell=True,
            cwd=pathlib.Path("./log/")
        )

    @staticmethod
    def set_current_working_directory() -> None:
        """
        Set the current working directory

        The following tasks are accomplished
        - Set the current working directory of this script to the directory in which this file exists (i.e. the base directory of the installer)

        Parameters
        ----------
        None

        Returns
        -------
        Nothing 
        """

        os.chdir(  # change the current working directory to
            os.path.dirname(  # get the directory's name for the file
                os.path.realpath(  # get the full path of
                    __file__  # the current file
                )
            )
        )

    @staticmethod
    def get_time_stamp() -> str:
        """
        Returns the timestamp in a particular format
        Example: 2020-01-31_23_58_59_GMT

        Parameters
        ----------
        None

        Returns 
        -------
        timestamp: str
            The timestamp in a particular format. Example: 2020-01-31_23_58_59_GMT
        """

        time_stamp: str = datetime.datetime.now().strftime("%Y-%m-%d_%H_%M_%S_")
        time_zone: Optional[str] = datetime.datetime.now(datetime.timezone.utc).astimezone().tzname()
        
        if time_zone is None:
            time_zone = ""

        return (
            time_stamp + time_zone
        )

# THE EXECUTION STARTS HERE
if __name__ == '__main__':
    FenixISOBuilder()
