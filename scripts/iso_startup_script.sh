#!/usr/bin/env bash

set -e -u

sed -i 's/#\(en_US\.UTF-8\)/\1/' /etc/locale.gen
locale-gen

sed -i "s/#Server/Server/g" /etc/pacman.d/mirrorlist

#=============#
# Custom code #
#=============#
sudo -S mount -o remount,size=1G /run/archiso/cowspace

gsettings set org.gnome.desktop.interface gtk-theme "Adapta-Nokto"

mkdir -p /etc/lightdm
touch /etc/lightdm/lightdm.conf
cat>/etc/lightdm/lightdm.conf<< EOL
[LightDM]
run-directory=/run/lightdm
[Seat:*]
session-wrapper=/etc/lightdm/Xsession
autologin-guest=false
autologin-user=root
autologin-user-timeout=0
greeter-session = lightdm-webkit2-greeter
greeter-show-manual-login=false
greeter-hide-users=true
allow-guest=false
user-session=budgie-desktop
[XDMCPServer]
[VNCServer]
EOL
groupadd -r autologin
groupadd -r nopasswdlogin
gpasswd -a root autologin
gpasswd -a root nopasswdlogin

systemctl set-default graphical.target
systemctl enable lightdm
systemctl start lightdm.service
mv /etc/skel/xinitrc /etc/X11/xinit/xinitrc

systemctl enable vboxservice.service
systemctl start vboxservice.service

systemctl enable NetworkManager.service
systemctl start NetworkManager.service

sudo reflector

VBoxClient --clipboard
VBoxClient --draganddrop
VBoxClient --seamless
VBoxClient --checkhostversion
VBoxClient --vmsvga

yes n | sudo pacman -Syu

gsettings set org.gnome.desktop.interface gtk-theme "Adapta-Nokto"


