#! /usr/bin/env sh

# Clean pipenv
pipenv uninstall fenix-library-running fenix-library-configuration fenix-library-analyzing fenix-library-installing elevate sphinx sphinx-rtd-theme sphinxcontrib-mermaid 
sudo pipenv uninstall fenix-library-running fenix-library-configuration fenix-library-analyzing fenix-library-installing elevate sphinx sphinx-rtd-theme sphinxcontrib-mermaid 
pipenv clean
sudo pipenv clean
pipenv --rm
sudo pipenv --rm

# Check if pipenv is cleaned
pipenv graph
sudo pipenv graph

# Regenerate pipenv
sudo pipenv install fenix-library-running fenix-library-configuration fenix-library-analyzing fenix-library-installing elevate  
sudo pipenv install --dev sphinx sphinx-rtd-theme sphinxcontrib-mermaid

# Check if pipenv is regenerated
pipenv graph
sudo pipenv graph